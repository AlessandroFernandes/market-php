<?php 
include"components/cabecalho.php";
include"config/controle-banco.php";
include"config/categoria-banco.php";

$id = $_GET['id'];
$categorias = listarCategoria($conexao);
$produto = buscarProduto($conexao, $id);

$checado = $produto['usado'] == true ? "checked=checked" : "";
?>
    <form action="alterar-produto.php"  class="mt-5" method="POST">
        <h1>Adiciona Produto</h1>
        <input type="hidden" id="id" name="id" value="<?= $produto['id'] ?>">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" placeholder="Nome" id="nome" name="nome" value="<?= $produto['nome'] ?>"> 
        </div>
        <div class="form-group">
            <label>Preço</label>
            <input type="number" class="form-control" placeholder="Preço" id="preco" name="preco" value="<?= $produto['preco'] ?>"> 
        </div>
        <div class="form-group">
            <label>Descrição</label>
            <textarea id="descricao" name="descricao" class="form-control" rows="3"><?= $produto['descricao'] ?></textarea>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" name="usado" value="true" <?= $checado ?>>
            <label class="form-check-label">Usado</label>
        </div>
        <div class="form-group">
            <label>Categoria</label>
            <select class="form-control" name="fk_categoria">
                <?php foreach($categorias as $categoria) : 
                        $selecionado = $produto['fk_categoria'] == $categoria['id'] ? "selected=selected" : "";
                    ?>
                    <option value="<?= $categoria['id']?>" <?= $selecionado ?>>
                        <?= $categoria['nome'] ?>
                <?php endforeach ?>
            </select>
        </div>
        <button  class="btn btn-primary" type="submit">Enviar</button>
    </form>

<?php include"component/rodape.php" ?>