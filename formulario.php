<?php 
include"components/cabecalho.php";
include"config/categoria-banco.php";

$categorias = listarCategoria($conexao);
?>
  
    <form action="adiciona-produto.php"  class="mt-5" method="POST">
        <h1>Adiciona Produto</h1>
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" placeholder="Nome" id="nome" name="nome"> 
        </div>
        <div class="form-group">
            <label>Preço</label>
            <input type="number" class="form-control" placeholder="Preço" id="preco" name="preco"> 
        </div>
        <div class="form-group">
            <label>Descrição</label>
            <textarea id="descricao" name="descricao" class="form-control" rows="3"></textarea>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" name="usado" value="true">
            <label class="form-check-label">Usado</label>
        </div>
        <div class="form-group">
            <label>Categoria</label>
            <select class="form-control" name="fk_categoria">
                <?php foreach($categorias as $categoria) : ?>
                    <option value="<?= $categoria['id'] ?>">
                        <?= $categoria['nome'] ?>
                <?php endforeach ?>
            </select>
        </div>
        <button  class="btn btn-primary" type="submit">Enviar</button>
    </form>

<?php include"component/rodape.php" ?>