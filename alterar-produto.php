<?php 
include"components/cabecalho.php";
include"config/controle-banco.php";

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $preco = $_POST['preco'];
        $descricao = $_POST['descricao'];
        $fk_categoria = $_POST['fk_categoria'];
        if(array_key_exists('usado', $_POST)){
            $usado = "true";
        }else{
            $usado = "false";
        }

    if(alterarProduto($conexao,$id, $nome, $preco, $descricao, $fk_categoria, $usado)){ ?>
        <p class='text-center alert alert-success mt-5'>Produto <?= $nome ?> <?= $preco ?> alterado com sucesso</p>
    <?php  } else { ?>
        <p class="text-center alert alert-danger mt-5">Produto <?= $nome ?> não alterado </p>
    <?php
    }

    ?>


<?php include"component/rodape.php" ?>