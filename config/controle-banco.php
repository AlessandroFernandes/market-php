<?php
include"conect.php";

function insereProduto($conexao, $nome, $preco, $descricao, $fk_categoria, $usado){
    return mysqli_query($conexao, "INSERT INTO produtos(nome, preco, descricao, fk_categoria, usado) VALUES('{$nome}', {$preco}, '{$descricao}', {$fk_categoria}, {$usado})");
}

function alterarProduto($conexao, $id, $nome, $preco, $descricao, $fk_categoria, $usado){
    return mysqli_query($conexao, "UPDATE produtos SET nome = '{$nome}', preco = {$preco}, descricao = '{$descricao}', fk_categoria = {$fk_categoria}, usado = {$usado}
                                   WHERE id = {$id}");
}

function listar($conexao){
    $produtos = [];
    $resultado = mysqli_query($conexao, "SELECT produtos.id, produtos.nome, produtos.preco, 
                                         produtos.descricao, produtos.usado, categorias.nome as categoria FROM produtos, categorias
                                         WHERE produtos.fk_categoria = categorias.id");

    while($produto = mysqli_fetch_assoc($resultado)){
          $produtos[] = $produto;
    }
    return $produtos;
 }

function deleta($conexao, $id){
    return mysqli_query($conexao, "DELETE from produtos where id = {$id}");
};

function buscarProduto($conexao, $id){
    $query = mysqli_query($conexao, "SELECT * FROM produtos WHERE id = {$id}");
    return mysqli_fetch_assoc($query);
};
