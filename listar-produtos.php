<?php 
include"components/cabecalho.php";
include"config/controle-banco.php";

?>

<h1 class="text-center mt-5"> Lista de Produtos</h1>

<?php if(array_key_exists("deletado", $_GET) && $_GET['deletado']=='true'): ?>
            <p class="text-success text-center">Produto deletado com sucesso!</p>
<?php endif?>

<table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th>Produto</th>
                <th>Preço</th>
                <th>Descrição</th>
                <th>Categoria</th>
                <th>Usado</th>
                <th><th>
            </tr>
        </thead>
        <tbody>
   <?php foreach(listar($conexao) as $produto) : ?>
            <tr>
                <td><?= $produto['nome'] ?></td>
                <td><?= $produto['preco'] ?></td>
                <td><?= substr($produto['descricao'],0,40) ?></td>
                <td><?= $produto['categoria'] ?></td>
                <td>
                    <?php if($produto['usado'] == true) : ?>
                               Usado
                           <?php else : ?>
                               Não usado
                    <?php endif ?>
                </td>
                <td>
                    <button><a href="editar-produto.php?id=<?= $produto['id'] ?>"><i class="far fa-edit"></i></a></button>
                </td>
                <td>
                    <form action="config/deleta-produto.php" method="POST">
                        <input type="hidden" name="id" value=<?= $produto['id'] ?>>
                        <button><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
    <?php endforeach ?>
        </tbody>
</table>

<?php include"component/rodape.php" ?>